/*
- Написати функцію `createNewUser()`, яка буде створювати та повертати об'єкт `newUser`.
- При виклику функція повинна запитати у імені, що викликає, і прізвище.
- Використовуючи дані, введені користувачем, створити об'єкт `newUser` з властивостями `firstName` та `lastName`.
- Додати в об'єкт `newUser` метод `getLogin()`, який повертатиме першу літеру імені користувача, з'єднану з прізвищем користувача, все в нижньому регістрі (наприклад, `Ivan Kravchenko → ikravchenko`).
- Створити користувача за допомогою функції createNewUser(). Викликати у користувача функцію `getLogin()`. Вивести у консоль результат виконання функції.

*/
class CreateNewUser {
   constructor(name, lname) {
      this.firsName = name;
      this.lastName = lname;
   }
   getLogin() {
      console.log((this.firsName[0] + this.lastName).toLowerCase());
      
   }

}

let newUser = new CreateNewUser('Ivan','Kravchenko');
